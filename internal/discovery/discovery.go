package discovery

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io/fs"
	"os"
	"path"
	"path/filepath"

	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/deps"
)

type DependencyFile struct {
	FileName      string
	Found         bool
	FoundPath     string
	DirPath       string
	DepType       deps.Type
	SearchSubdirs bool
}

func (df DependencyFile) String() string {
	return fmt.Sprintf("FileName: %s, Found: %t, FoundPath: %s, DirPath: %s, DepType: %s", df.FileName, df.Found, df.FoundPath, df.DirPath, deps.TypeDescription(df.DepType))
}

// LocateFile searches for a file with the given name in the specified directory.
// If searchSubdirs is set to true, it recursively searches subdirectories.
func LocateFile(dir, fileName string, searchSubdirs bool) ([]string, error) {
	if !searchSubdirs {
		fp := path.Join(dir, fileName)

		_, err := os.Stat(fp)
		if os.IsNotExist(err) {
			return []string{}, err
		}

		return []string{fp}, nil
	}

	matches := []string{}

	err := filepath.WalkDir(dir, func(walkfile string, info fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			fileMatches, err := filepath.Glob(path.Join(walkfile, fileName))
			if err != nil {
				return err
			}
			matches = append(matches, fileMatches...)
		}
		return nil
	})

	return matches, err
}

func FileChecksum(filePath string) string {
	data, err := os.ReadFile(filePath)
	if err != nil {
		return ""
	}

	hash := sha256.Sum256(data)

	return hex.EncodeToString(hash[:])
}
