from conan import ConanFile

class MyProject(ConanFile):
    requires=("boost/1.76.0", "opencv/4.6.0",
      "poco/1.11.0", "gtest/1.11.0",
      "catch2/3.0.0")

    # Binary configuration
    settings = "os", "compiler", "build_type", "arch"
    generators = "CMakeToolchain", "CMakeDeps"

    def requirements(self):
        self.requires("zlib/1.2.11")
        self.requires("eigen/3.3.9@conan/stable")

        # Add base64 dependency for Windows
        if self.settings.os == "Windows":
            self.requires("base64/0.4.0")

    def build_requirements(self):
        # Use the system's CMake for Windows
        if self.settings.os != "Windows":
            self.tool_requires("cmake/3.22.6")
