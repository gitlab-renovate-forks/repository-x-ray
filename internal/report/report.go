package report

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/deps"
)

const libsDir = "libs"

type Report struct {
	ScannerVersion string            `json:"scannerVersion"`
	FileName       string            `json:"fileName,omitempty"`
	FileChecksum   string            `json:"checksum,omitempty"`
	Libs           []deps.Dependency `json:"libs"`
	dstDir         string
	dstFile        string
}

func New(dstDir, dstFile, version, fileName, sha string) *Report {
	return &Report{
		ScannerVersion: version,
		FileName:       fileName,
		FileChecksum:   sha,
		dstDir:         dstDir,
		dstFile:        dstFile,
	}
}

// Save saves report to a destination file
func (r *Report) Save() error {
	err := r.mkDir()
	if err != nil {
		return fmt.Errorf("failed to save report: %v", err)
	}

	jsonData, err := json.Marshal(r)
	if err != nil {
		return fmt.Errorf("failed to marshal report to JSON: %v", err)
	}

	dst := filepath.Join(r.dstDir, libsDir, r.dstFile)

	// Replace escaped Unicode sequences
	jsonStr := string(jsonData)
	jsonStr = strings.ReplaceAll(jsonStr, "\\u003e", ">")

	err = os.WriteFile(dst, []byte(jsonStr), 0o644)
	if err != nil {
		return fmt.Errorf("failed to write report to file: %v", err)
	}

	return nil
}

// mkDir creates reports directory
func (r *Report) mkDir() error {
	err := os.MkdirAll(filepath.Join(r.dstDir, libsDir), 0o755)
	if err != nil {
		return fmt.Errorf("failed to create reports directory: %v", err)
	}

	return nil
}
