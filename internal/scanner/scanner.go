package scanner

import (
	"bufio"
	"encoding/json"
	"encoding/xml"
	"os"
	"path/filepath"
	"regexp"
	"slices"
	"strings"

	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/deps"
	"gopkg.in/yaml.v3"
)

type packageJSON struct {
	Dependencies    map[string]string `json:"dependencies"`
	DevDependencies map[string]string `json:"devDependencies"`
}

type environmentYAML struct {
	Dependencies []yaml.Node `yaml:"dependencies"`
}

type composerJSON struct {
	Dependencies    map[string]string `json:"require"`
	DevDependencies map[string]string `json:"require-dev"`
}

type pomXML struct {
	Dependencies []struct {
		Name    string `xml:"artifactId"`
		Version string `xml:"version"`
	} `xml:"dependencies>dependency"`
}

type csProjectXML struct {
	PackageReferences []struct {
		Include string `xml:"Include,attr"`
		Version string `xml:"Version,attr"`
	} `xml:"ItemGroup>PackageReference"`
}

func Scan(depFile string, depType deps.Type) ([]deps.Dependency, error) {
	d := []deps.Dependency{}
	file, err := os.Open(depFile)
	if err != nil {
		return d, err
	}
	defer file.Close()

	switch depType {
	case deps.Ruby:
		d = scanGemfileLock(file)
	case deps.JavaScript:
		d, err = scanPackageJSON(file)
	case deps.Go:
		d = scanGoMod(file)
	case deps.PythonPoetry:
		d = scanPythonPoetry(file)
	case deps.PythonPip:
		processedFiles := []string{}
		// the starting point is just one file
		filesToProcess := []string{depFile}
		for len(filesToProcess) > 0 {
			// process the first file in queue and pop the first file
			currentFile := filesToProcess[0]
			filesToProcess = filesToProcess[1:]

			// currentDeps are the plain deps, otherFiles are the recursive files
			file, err = os.Open(currentFile)
			if err != nil {
				return d, err
			}
			currentDeps, otherFiles := scanPythonPip(currentFile, file)
			file.Close() // in for loop, thus close early, defer will accumulate opened files
			d = append(d, currentDeps...)

			// add the processed file to processedFiles to avoid duplication
			processedFiles = append(processedFiles, currentFile)
			for _, otherFile := range otherFiles {
				if !slices.Contains(processedFiles, otherFile) {
					filesToProcess = append(filesToProcess, otherFile)
				}
			}
		}

	case deps.PythonConda:
		d, err = scanPythonConda(file)
	case deps.PHP:
		d, err = scanPHPComposer(file)
	case deps.JavaMaven:
		d, err = scanPomXML(file)
	case deps.JavaGradle, deps.KotlinGradle:
		d = scanBuildGradle(file)
	case deps.CSharp:
		d, err = scanCSharpProject(file)
	case deps.CppConanTxt:
		d = scanCppConanTxt(file)
	case deps.CppConanPy:
		d = scanCppConanPy(file)
	case deps.CppVcpkg:
		d, err = scanVcpkgJSON(file)
	default:
	}

	if err != nil {
		return d, err
	}

	return d, nil
}

func scanGemfileLock(file *os.File) []deps.Dependency {
	scanner := bufio.NewScanner(file)
	gems := []deps.Dependency{}

	foundDependencies := false
	for scanner.Scan() {
		line := scanner.Text()

		// Looking for the DEPENDENCIES section
		if !foundDependencies && line == "DEPENDENCIES" {
			foundDependencies = true
			continue
		}

		// Stop scanning if we reached the end of the DEPENDENCIES section
		if foundDependencies && line == "" {
			break
		}

		if foundDependencies {
			gems = append(gems, deps.NewRubyGem(line))
		}
	}

	return gems
}

func scanPackageJSON(file *os.File) ([]deps.Dependency, error) {
	jsdeps := []deps.Dependency{}

	fileByteSlice, err := os.ReadFile(file.Name())
	if err != nil {
		return jsdeps, err
	}

	var pj packageJSON
	err = json.Unmarshal(fileByteSlice, &pj)
	if err != nil {
		return jsdeps, err
	}

	for name, version := range pj.Dependencies {
		jsdeps = append(jsdeps, deps.NewDependency(name, version))
	}

	for name, version := range pj.DevDependencies {
		jsdeps = append(jsdeps, deps.NewDependency(name, version))
	}

	return jsdeps, nil
}

func scanGoMod(file *os.File) []deps.Dependency {
	scanner := bufio.NewScanner(file)
	godeps := []deps.Dependency{}

	foundDependencies := false
	for scanner.Scan() {
		line := scanner.Text()

		// Looking for the first `require` section
		if !foundDependencies && strings.HasPrefix(line, "require") {
			foundDependencies = true

			if strings.HasPrefix(line, "require (") {
				continue
			}

			// If the require section is on the same line as the `require` keyword
			// then there is only one package
			return []deps.Dependency{
				deps.NewGoPackage(strings.TrimPrefix(line, "require ")),
			}
		}

		// Stop scanning if we reached the end of the require section
		if foundDependencies && line == ")" {
			break
		}

		if foundDependencies {
			godeps = append(godeps, deps.NewGoPackage(line))
		}
	}

	return godeps
}

func scanPythonPoetry(file *os.File) []deps.Dependency {
	scanner := bufio.NewScanner(file)
	foundDependencies := false
	pdeps := []deps.Dependency{}

	for scanner.Scan() {
		line := scanner.Text()

		depGrp := strings.HasPrefix(line, "[tool.poetry") && strings.HasSuffix(line, "dependencies]")
		if !foundDependencies && depGrp {
			foundDependencies = true
			continue
		}

		// Stop scanning the current group if we reached the end of the dependency section
		// And allow to continue scanning if there are more groups
		if foundDependencies && line == "" {
			foundDependencies = false
			continue
		}

		if foundDependencies {
			pdeps = append(pdeps, deps.NewPythonPoetryPackage(line))
		}
	}

	return pdeps
}

func scanPythonPip(filePath string, file *os.File) ([]deps.Dependency, []string) {
	scanner := bufio.NewScanner(file)
	pdeps := []deps.Dependency{}
	includedRequirementsFiles := []string{}

	for scanner.Scan() {
		// if line is empty or line start with #, continue
		line := scanner.Text()
		line = strings.TrimSpace(line)
		if line == "" || strings.HasPrefix(line, "#") {
			continue
		}

		// split by the first #, if the left part is empty, continue
		parts := strings.Split(line, "#")
		if len(parts[0]) == 0 {
			continue
		}
		raw := strings.TrimSpace(parts[0])

		if strings.HasPrefix(raw, "-r") {
			// process -r other_requirements.txt, extract file path
			re := regexp.MustCompile(`^-r\s+(\S*)`)
			matches := re.FindStringSubmatch(raw)

			if len(matches) == 2 {
				// relative path of the target file from the current file
				otherRequirementsFile := matches[1]
				// current file's dir path
				curDir := filepath.Dir(filePath)
				// path to target file
				otherRequirementsFilePath := filepath.Join(curDir, otherRequirementsFile)
				includedRequirementsFiles = append(includedRequirementsFiles, otherRequirementsFilePath)
			}
		} else {
			// normal workflow
			pdeps = append(pdeps, deps.NewPythonPipPackage(raw))
		}
	}

	return pdeps, includedRequirementsFiles
}

func scanPythonConda(file *os.File) ([]deps.Dependency, error) {
	pdeps := []deps.Dependency{}

	fileByteSlice, err := os.ReadFile(file.Name())
	if err != nil {
		return pdeps, err
	}

	var e environmentYAML
	err = yaml.Unmarshal(fileByteSlice, &e)
	if err != nil {
		return pdeps, err
	}

	for _, node := range e.Dependencies {
		if node.ShortTag() != "!!str" {
			continue
		}

		pdeps = append(pdeps, deps.NewPythonCondaPackage(node.Value))
	}

	return pdeps, nil
}

func scanPHPComposer(file *os.File) ([]deps.Dependency, error) {
	res := []deps.Dependency{}

	fileByteSlice, err := os.ReadFile(file.Name())
	if err != nil {
		return res, err
	}

	var cj composerJSON
	err = json.Unmarshal(fileByteSlice, &cj)
	if err != nil {
		return res, err
	}

	for name, version := range cj.Dependencies {
		res = append(res, deps.NewDependency(name, version))
	}

	for name, version := range cj.DevDependencies {
		res = append(res, deps.NewDependency(name, version))
	}

	return res, nil
}

func scanPomXML(file *os.File) ([]deps.Dependency, error) {
	res := []deps.Dependency{}

	fileByteSlice, err := os.ReadFile(file.Name())
	if err != nil {
		return res, err
	}

	var px pomXML
	err = xml.Unmarshal(fileByteSlice, &px)
	if err != nil {
		return res, err
	}

	for _, dep := range px.Dependencies {
		res = append(res, deps.NewDependency(dep.Name, dep.Version))
	}

	return res, nil
}

func scanBuildGradle(file *os.File) []deps.Dependency {
	scanner := bufio.NewScanner(file)
	jdeps := []deps.Dependency{}

	foundDependencies := false
	for scanner.Scan() {
		line := scanner.Text()

		// Looking for the first `dependencies` section
		if !foundDependencies && strings.HasPrefix(line, "dependencies {") {
			foundDependencies = true
			continue
		}

		// Stop scanning if we reached the end of the dependencies section
		if foundDependencies && line == "}" {
			break
		}

		if foundDependencies {
			d, err := deps.ParseGradleDependency(line)
			if err != nil {
				continue
			}

			jdeps = append(jdeps, d)
		}
	}

	return jdeps
}

func scanCSharpProject(file *os.File) ([]deps.Dependency, error) {
	res := []deps.Dependency{}

	fileByteSlice, err := os.ReadFile(file.Name())
	if err != nil {
		return res, err
	}

	var cs csProjectXML
	err = xml.Unmarshal(fileByteSlice, &cs)
	if err != nil {
		return res, err
	}

	for _, dep := range cs.PackageReferences {
		res = append(res, deps.NewDependency(dep.Include, dep.Version))
	}

	return res, nil
}

func scanCppConanTxt(file *os.File) []deps.Dependency {
	scanner := bufio.NewScanner(file)
	foundDependencies := false
	pdeps := []deps.Dependency{}

	for scanner.Scan() {
		line := scanner.Text()

		if !foundDependencies && strings.HasPrefix(line, "[requires]") {
			foundDependencies = true
			continue
		}

		// Ignoring blank line
		if foundDependencies && line == "" {
			continue
		}

		// Ignoring a comment line
		if foundDependencies && strings.HasPrefix(line, "#") {
			continue
		}

		// Stop scanning the [requires] group if we reached the end of the dependency section
		if foundDependencies && strings.HasPrefix(line, "[") {
			foundDependencies = false
			continue
		}

		if foundDependencies {
			pdeps = append(pdeps, deps.ParseCppConanTxtDependency(line))
		}
	}

	return pdeps
}

func scanCppConanPy(file *os.File) []deps.Dependency {
	scanner := bufio.NewScanner(file)
	foundDependencies := false
	pdeps := []deps.Dependency{}
	requiresPrefix := "requires=("

	for scanner.Scan() {
		line := scanner.Text()
		tline := strings.TrimSpace(line)

		// Looking for `self.requires("zlib/1.2.11")` format
		if !foundDependencies && strings.HasPrefix(tline, "self.requires(") {
			pdeps = append(pdeps, deps.ParseCppConanPyDependency(tline))
			continue
		}

		// Looking for `requires = ( ... )` format
		if depsString, found := strings.CutPrefix(
			strings.ReplaceAll(tline, " ", ""),
			requiresPrefix,
		); found && !foundDependencies {
			pdeps = append(pdeps, deps.ParseCppConanPyDependencies(depsString)...)
			foundDependencies = !strings.HasSuffix(tline, ")")
			continue
		}

		// Ignoring blank line
		if foundDependencies && tline == "" {
			continue
		}

		// Ignoring a comment line
		if foundDependencies && strings.HasPrefix(tline, "#") {
			continue
		}

		// Stop scanning the requires group if we reached the end of the dependency section
		if foundDependencies && tline == ")" {
			foundDependencies = false
			continue
		}

		if foundDependencies {
			pdeps = append(pdeps, deps.ParseCppConanPyDependencies(tline)...)
			foundDependencies = !strings.HasSuffix(tline, ")")
		}
	}

	return pdeps
}

func scanVcpkgJSON(file *os.File) ([]deps.Dependency, error) {
	cppdeps := []deps.Dependency{}

	var data struct {
		Dependencies     any `json:"dependencies"`
		TestDependencies any `json:"test-dependencies"`
	}

	fileByteSlice, err := os.ReadFile(file.Name())
	if err != nil {
		return cppdeps, err
	}

	err = json.Unmarshal(fileByteSlice, &data)
	if err != nil {
		return cppdeps, err
	}

	dependencies := data.Dependencies.([]any)
	dependencies = append(dependencies, data.TestDependencies.([]any)...)

	for _, dependency := range dependencies {
		var name, version string

		switch d := dependency.(type) {
		case string:
			parts := strings.Split(d, "@")
			name = parts[0]
			if len(parts) == 2 {
				version = parts[1]
			}

		case map[string]any:
			var versionExists bool
			name = d["name"].(string)
			version, versionExists = d["version"].(string)
			if !versionExists {
				version = ""
			}

		default:
		}

		cppdeps = append(cppdeps, deps.NewDependency(name, version))
	}

	return cppdeps, nil
}
