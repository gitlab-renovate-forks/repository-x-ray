# Repository X-Ray

Repository X-Ray is a tool designed to scan a specified project directory for configuration files associated with various package managers.
It extracts information about configured dependencies from these package managers.
Once the list of dependencies is obtained, X-Ray fetches detailed descriptions for each dependency.
After collecting descriptions for all dependencies, X-Ray compiles and stores the list along with their descriptions in report files.
These report files are organized based on the programming language of the project.

Generated reports are used later by [GitLab Duo Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/index.html) by providing additional context to improve the accuracy and relevance of code recommendations.


## Supported languages and package managers

| Language   | Package Manager | Configuration File   |
| ---------- |-----------------| -------------------- |
| Go         | Go Modules      | `go.mod`             |
| JavaScript | NPM, Yarn       | `package.json`       |
| Ruby       | RubyGems        | `Gemfile.lock`       |
| Python     | Poetry          | `pyproject.toml`     |
| Python     | Pip             | `requirements.txt`   |
| Python     | Conda           | `environment.yml`    |
| PHP        | Composer        | `composer.json`      |
| Java       | Maven           | `pom.xml`            |
| Java       | Gradle          | `build.gradle`       |
| Kotlin     | Gradle          | `build.gradle.kts`   |
| C#         | NuGet           | `*.csproj`           |
| C/C++      | Conan           | `conanfile.txt`      |
| C/C++      | Conan           | `conanfile.py`       |
| C/C++      | vcpkg           | `vcpkg.json`         |

## Warning

This project is deprecated and scheduled for closure.
The X-Ray CI job, which runs an image from this project, was [deprecated](https://gitlab.com/gitlab-org/gitlab/-/issues/500146) in GitLab 17.6 and will be removed in GitLab 18.0. This repository is obsolete as project dependencies are now scanned automatically on new commits pushed to the default branch. More details [here.](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/repository_xray.html#how-repository-x-ray-works)

No further development or maintenance will occur on this repository.

## Development setup

1. (Optional) The project uses `golangci-lint` as a linter. Which is also the part of `make audit` task. Please refer to the [installation instructions](https://golangci-lint.run/welcome/install/#local-installation) to install it.

## How to run locally

1. Run `make run_all` to generate reports for all dependency managers.
1. Run `go run ./cmd/scan -o reports -p <PROJECT-PATH>` to generate reports for a specific project.

## How to run an X-Ray scan as a CI Job with GDK

1. Make sure you have [GitLab Development Kit (GDK)](https://gitlab.com/gitlab-org/gitlab-development-kit) configured on your machine.
1. Make sure you run GDK on a loopback interface. [Follow the instructions](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/local_network.md#create-loopback-interface) to set it up.
1. Create a new repository or use an existing one with any package manager.
1. Add the X-Ray scan configuration to the `.gitlab-ci.yml` file in the repository with the following content:
    ```yaml
    stages:
    - x-ray

    xray_scan:
      stage: x-ray
      image: registry.gitlab.com/gitlab-org/code-creation/repository-x-ray:latest
      variables:
        OUTPUT_DIR: reports
      allow_failure: true
      script:
        - x-ray-scan -p "$CI_PROJECT_DIR" -o "$OUTPUT_DIR"
      artifacts:
        # this line uses xray_scan job output as source for GitLab Rails code gen feature
        reports:
          repository_xray: "$OUTPUT_DIR/*/*.json"
        # this line saves xray_scan job output in raw form for inspection for testing purposes
        paths:
        - "$OUTPUT_DIR/*/*.json"
    ```
1. Configure the GitLab Runner to run CI jobs. [Follow the instructions](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/main/doc/howto/runner.md#executing-a-runner-directly-on-your-workstation). Make sure to use the "docker" type of runner; "shell" won't be able to pull the X-Ray docker image.
1. In addition, add `extra_hosts` variable to the `[runners.docker]` section of your GitLab Runner's config. The config usually located in `~/.gitlab-runner/config.toml`. Restart the runner afterwards.
    ```toml
      [runners.docker]
        extra_hosts = ["gdk.test:172.16.123.1"]
    ```
1. Make sure [AI Gateway](https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist#how-to-run-the-server-locally) is running.
1. Find and restart the X-Ray CI job.
1. To verify, you can check the CI job's log. Additionally, you can call `Projects::XrayReport.last` from the `rails console`. It should return a newly created record.

## Dogfooding

This repository itself uses X-Ray scan feature by defining `xray_scan` job in its `.gitlab-ci.yml` config file. It is possible to preview raw content of X-Ray scan report by viewing `xray_scan` job attached artifacts. You can read more about `artifacts: reports` and CI job configuration at [dedicated GitLab documentation page](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsrepository_xray).

## Release Process

In order to provide more control over the release process that distributes changes to the X-Ray scanner to GitLab users, this repository follows the release process defined below:

1. On feature branches, `build` CI jobs create `registry.gitlab.com/gitlab-org/code-creation/repository-x-ray/dev:$CI_COMMIT_SHORT_SHA` images that are used for end-to-end testing purposes.

2. To release a new version of the scanner, a new [Git tag](https://docs.gitlab.com/ee/user/project/repository/tags/#create-a-tag) must be created.

3. The `build` CI job that runs on Git tags creates the `registry.gitlab.com/gitlab-org/code-creation/repository-x-ray` image and tags it as `rc`.

4. Images with the `rc` tag are used by [`gitlab-org/gitlab`](https://gitlab.com/gitlab-org/gitlab) and this project for testing. 

5. After one business day of using the new `rc` image, a manual `release` job is created that tags the `rc` image as `latest` to make it available to all GitLab users.

## Versioning

This project follows the [SemVer](https://semver.org) specification.

## Roadmap

The overarching idea behind the repository X-Ray feature is to build comperhensive [RAG](https://www.promptingguide.ai/techniques/rag) tool that will provide relevant context information sourced from the repository (e.g. code snippets) for code creation requests.


To track progress on this project, you can refer to [this epic](https://gitlab.com/groups/gitlab-org/-/epics/11733) that captures the high-level vision and open issues that represent smaller iterations towards that goal.

## Contributing

See [CONTRIBUTING](CONTRIBUTING.md) file.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License

See [LICENSE](LICENSE)
