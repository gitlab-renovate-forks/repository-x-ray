package xlog

import (
	"context"
	"fmt"
	"log/slog"
	"runtime"
	"time"
)

// Debug prints a formatter message to log with LevelDebug
func Debug(format string, args ...any) {
	log(slog.LevelDebug, format, args...)
}

// DebugPrefix prints a formatter message to log with LevelDebug and prefix
func DebugPrefix(prefix, format string, args ...any) {
	Debug("[%s] %s", prefix, fmt.Sprintf(format, args...))
}

// Error prints a formatter message to log with LevelError
func Error(format string, args ...any) {
	log(slog.LevelError, format, args...)
}

// ErrorPrefix prints a formatter message to log with LevelError and prefix
func ErrorPrefix(prefix, format string, args ...any) {
	Error("[%s] %s", prefix, fmt.Sprintf(format, args...))
}

// Info prints a formatter message to log with LevelInfo
func Info(format string, args ...any) {
	log(slog.LevelInfo, format, args...)
}

// InfoPrefix prints a formatter message to log with LevelInfo and prefix
func InfoPrefix(prefix, format string, args ...any) {
	Info("[%s] %s", prefix, fmt.Sprintf(format, args...))
}

// Warn prints a formatter message to log with LevelWarn
func Warn(format string, args ...any) {
	log(slog.LevelWarn, format, args...)
}

// WarnPrefix prints a formatter message to log with LevelWarn and prefix
func WarnPrefix(prefix, format string, args ...any) {
	Warn("[%s] %s", prefix, fmt.Sprintf(format, args...))
}

/*
From https://pkg.go.dev/golang.org/x/exp/slog#hdr-Wrapping_output_methodsInfo

The logger functions use reflection over the call stack to find the file name and
line number of the logging call within the application. This can produce incorrect
source information for functions that wrap slog. For instance, if you define this
function in file mylog.go:

```

	func Infof(format string, args ...any) {
		slog.Default().Info(fmt.Sprintf(format, args...))
	}

```

and you call it like this in main.go:

```

	Infof(slog.Default(), "hello, %s", "world")

```

then slog will report the source file as mylog.go, not main.go.

A correct implementation of Infof will obtain the source location (pc) and pass it to NewRecord.

Implementation example: https://pkg.go.dev/log/slog#example-package-Wrapping
*/
func log(logLevel slog.Level, format string, args ...any) {
	logger := slog.Default()

	if !logger.Enabled(context.Background(), logLevel) {
		return
	}
	var pcs [1]uintptr
	runtime.Callers(2, pcs[:]) // skip [Callers, Infof]
	r := slog.NewRecord(time.Now(), logLevel, fmt.Sprintf(format, args...), pcs[0])
	_ = logger.Handler().Handle(context.Background(), r)
}
