package xlog

import (
	"bytes"
	"log/slog"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestLogTextHandler(t *testing.T) {
	var buf bytes.Buffer

	l := slog.New(slog.NewTextHandler(&buf, nil))
	slog.SetDefault(l)

	checkLogOutput := func(want string) {
		t.Helper()
		require.Contains(t, buf.String(), want)
		buf.Reset()
	}

	// By default, debug messages are not printed.
	DebugPrefix("prefix", "%s %d", "msg", 1)
	require.Equal(t, buf.String(), "")
	buf.Reset()

	l = slog.New(slog.NewTextHandler(&buf, &slog.HandlerOptions{Level: slog.LevelDebug}))
	slog.SetDefault(l)

	DebugPrefix("prefix", "%s %d", "msg", 1)
	checkLogOutput(`level=DEBUG msg="[prefix] msg 1"`)

	ErrorPrefix("prefix", "%s %d", "msg", 1)
	checkLogOutput(`level=ERROR msg="[prefix] msg 1"`)

	InfoPrefix("prefix", "%s %d", "msg", 1)
	checkLogOutput(`level=INFO msg="[prefix] msg 1"`)

	WarnPrefix("prefix", "%s %d", "msg", 1)
	checkLogOutput(`level=WARN msg="[prefix] msg 1"`)
}
