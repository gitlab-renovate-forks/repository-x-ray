FROM golang:1.22.2

WORKDIR /usr/src/app

# pre-copy/cache go.mod for pre-downloading dependencies and only redownloading them in subsequent builds if they change
COPY go.mod go.sum ./
RUN go mod download && go mod verify

COPY . .
RUN make build BUILDLOC=/usr/local/bin APPNAME=x-ray-scan

ENV PATH "$PATH:/usr/src/app/"

CMD ["x-ray-scan"]